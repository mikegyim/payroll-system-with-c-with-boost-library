#include <QTest>
#include <salariedemployee.h>
#include <iostream>
Q_DECLARE_METATYPE(std::string)
class SalariedEmployeeTest : public QObject {
    Q_OBJECT
private slots:

void pay() {
    SalariedEmployee s("John Doe", SSN("123-45-6789") , 5000);
    QCOMPARE(s.pay(), 5000);}
void constructor_data() {
        QTest::addColumn<std::string>("name");
          QTest::addColumn<std::string>("ssn");
	QTest::addColumn<int>("salary");
	QTest::addColumn<int>("result");
	QTest::newRow("salary1")
		<< std::string("Breaking Bad") << std::string("123-45-6789") << 1250 << 1250;
	QTest::newRow("salary2")
		<< std::string("Harry Potter") << std::string("123-45-6789") << 7000 << 7000;
	QTest::newRow("salary3")
            << std::string("Coronavirus Vaccine") << std::string("123-45-6789") << 500 << 500;
    QTest::newRow("salary4")
            << std::string("Mosquito Bite") << std::string("123-45-6789") << 0 << 0;}
void constructor() {
	 QFETCH(std::string, name);
        QFETCH(std::string, ssn);
	QFETCH(int, salary);
	QFETCH(int, result);
	SalariedEmployee p(name, SSN(ssn), salary);
	QCOMPARE(p.pay(), result);
	QCOMPARE(p.salary(), result);}
};
QTEST_MAIN(SalariedEmployeeTest)
#include "salariedemployeetest.moc"
