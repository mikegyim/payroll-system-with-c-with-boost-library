#include <QTest>
#include <hourlyemployee.h>
#include <iostream>


Q_DECLARE_METATYPE(std::string)

class HourlyEmployeeTest: public QObject {
    Q_OBJECT

private slots:

void pay() {
    HourlyEmployee s(" John  Doe ", SSN("123-45-6789"),  35, 40);
    QCOMPARE(s.pay(), 1400);
}

void constructor_data() {
	QTest::addColumn<std::string>("name");
        QTest::addColumn<std::string>("ssn");
	QTest::addColumn<int>("rate");
	QTest::addColumn<int>("hours");
	QTest::addColumn<int>("result");
	QTest::newRow("rate")
		<< std::string("John Doe") <<std::string("123-45-6789") << 35 << 40 << 1400;
	QTest::newRow("rate2")
		<< std::string("John Doe") << std::string("123-45-6789") << 60 << 40 << 2400;
	QTest::newRow("rate3")
		<< std::string("John Doe") << std::string("123-45-6789") << 70 << 40 << 2800;
	QTest::newRow("hours")
		<< std::string("John Doe") << std::string("123-45-6789") << 25 << 45 << 1125;
	QTest::newRow("hours2")
		<< std::string("John Doe") <<  std::string("123-45-6789") << 25 << 55 << 1375;
	QTest::newRow("hours3")
		<< std::string("John Doe") <<  std::string("123-45-6789") << 25 << 80 << 2000;
    QTest::newRow("hours4")
            << std::string("John Doe") <<  std::string("123-45-6789") << 0 << 80 << 0;
    QTest::newRow("hours5")
            << std::string("John Doe") <<  std::string("123-45-6789") << 0 << 0 << 0;
    QTest::newRow("hours6")
            << std::string("John Doe") <<  std::string("123-45-6789") << 40 << 0 << 0;}
            void constructor() {
	QFETCH(std::string, name);
        QFETCH(std::string, ssn);
	QFETCH(int, rate);
	QFETCH(int, hours);
    QFETCH(int, result);
    HourlyEmployee m(name, SSN(ssn), rate, hours);
    QCOMPARE(m.pay(), result);}
};
QTEST_MAIN(HourlyEmployeeTest)
#include "hourlyemployeetest.moc"
