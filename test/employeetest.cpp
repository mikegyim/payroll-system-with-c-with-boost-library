#include <QTest>
#include <assertexcept.h>
#include <ssn.h>
#include <employee.h>
Q_DECLARE_METATYPE(std::string)
class MockEmployee : public Employee {
public:
    MockEmployee(const std::string& name, const SSN& ssn) :
            Employee(name, ssn) {}
            int pay() const override { return 0; }
};
class EmployeeTest : public QObject {
Q_OBJECT
private slots:
    void constructor_data() {
        QTest::addColumn<std::string>("name");
        QTest::addColumn<std::string>("ssn");
        QTest::newRow("typical")
                << std::string("John Doe")
                << std::string("123-45-6789");
        QTest::newRow("single letter")
                << std::string("J")
                << std::string("123-45-6789");
        QTest::newRow("no space")
                << std::string("JohnDoe")
                << std::string("123-45-6789");
        QTest::newRow("long name")
                << std::string("John Jacob Jingleheimer Schmidt")
                << std::string("123-45-6789");}
                void constructor() {
        QFETCH(std::string, name);
        QFETCH(std::string, ssn);
        MockEmployee m(name, SSN(ssn));
    }
    void constructor_xfail_data() {
        QTest::addColumn<std::string>("name");
        QTest::addColumn<std::string>("ssn");
        QTest::newRow("empty name")
                << std::string("")
                << std::string("123-45-6789");
        QTest::newRow("single space name")
                << std::string(" ")
                << std::string("123-45-6789");
        QTest::newRow("single tab")
                << std::string("\t")
                << std::string("123-45-6789");
        QTest::newRow("single newline")
                << std::string("\n")
                << std::string("123-45-6789");
        QTest::newRow("combination of whitespace")
                << std::string(" \n\t")
                << std::string("123-45-6789");
        QTest::newRow("invalid ssn")
                << std::string("John Doe")
                << std::string("999-99-9999");
    }
    void constructor_xfail() {
        QFETCH(std::string, name);
        QFETCH(std::string, ssn);
#ifdef NDEBUG
        // the second argument is to suppress a warning about variadic
        // macro arguments needing at least one argument
        QSKIP("Assertions disabled", "");
#endif
        QVERIFY_EXCEPTION_THROWN(MockEmployee m(name, SSN(ssn)),
                                 AssertException);}
                                 void name_data() {
        QTest::addColumn<std::string>("name");
        QTest::addColumn<std::string>("result");
        QTest::newRow("typical")
                << std::string("John Doe")
                << std::string("John Doe");}
                void name() {
        QFETCH(std::string, name);
        QFETCH(std::string, result);
        MockEmployee m(name, SSN("123-45-6789"));
        QCOMPARE(m.name(), result);}
};
QTEST_MAIN(EmployeeTest)
#include "employeetest.moc"