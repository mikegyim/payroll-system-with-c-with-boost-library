
// SPDX-License-Identifier: GPL-3.0-or-later
#include <employee.h>
#include <assertexcept.h>
#include <algorithm>
#include <cctype>
bool Employee::invariant() const{
    BOOST_ASSERT_MSG(!m_name.empty(), "Name not empty");
    BOOST_ASSERT_MSG(!std::all_of(m_name.begin(), m_name.end(), isspace),
                     "Name not only whitespace");return true;}
Employee::Employee(const std::string& name,  const SSN& ssn ): m_name(name),  m_ssn(ssn)
{//preconditions
    BOOST_ASSERT_MSG(!name.empty(), "Name not empty");
    BOOST_ASSERT_MSG(!std::all_of(name.begin(), name.end(), isspace),
                     "Name not only whitespace");
    //postconditions
BOOST_ASSERT_MSG(m_name == name, " Name Stored");
BOOST_ASSERT_MSG(m_ssn == ssn, "SSN stored");
//invariant
BOOST_ASSERT(invariant());}





    
