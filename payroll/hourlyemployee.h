

// SPDX-License-Identifier: GPL-3.0-or-later
#ifndef HOURLY_H
#define HOURLY_H
#include "employee.h"
#include "../ssn/ssn.h"
class HourlyEmployee : public Employee {
public:
    HourlyEmployee(const std::string& name,  const SSN& ssn,
            const int& rate, const int& hours);
    int pay() const override;
protected:
    bool invariant() const;
private:
    int m_rate;
    int m_hours;}; // class HourlyEmployee
    #endif
