
// SPDX-License-Identifier: GPL-3.0-or-later
#ifndef SALARIED_H
#define SALARIED_H
#include "employee.h"
class SalariedEmployee : public Employee {
public:
    SalariedEmployee(const std::string& name, const SSN& ssn, const int& salary);
    int salary() const {return m_salary;}
    int pay() const override;
protected:
    bool invariant() const;
private:
    int m_salary;};
#endif
