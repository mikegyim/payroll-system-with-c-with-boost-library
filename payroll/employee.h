

// SPDX-License-Identifier: GPL-3.0-or-later
#ifndef EMPLOYEE_H
#define EMPLOYEE_H
#include  <string>
#include "../ssn/ssn.h"
class
  Employee {
public:
    Employee(const std::string& name, const SSN& );
    virtual ~Employee() {}
    virtual int pay() const = 0;
  std::string name() const {return m_name;}
protected:
    bool invariant() const;
private:
    std::string m_name;
    //std:: Pay_Type;
    SSN m_ssn;
}; // class Employee
#endif // EMPLOYEE_H
