
// SPDX-License-Identifier: GPL-3.0-or-later
#include <hourlyemployee.h>
#include <assertexcept.h>
bool HourlyEmployee::invariant() const {
    BOOST_ASSERT(Employee::invariant());
    BOOST_ASSERT_MSG(m_rate>=0, "rate");
    BOOST_ASSERT_MSG(m_hours>=0, "hours");
    return true;}
    HourlyEmployee::HourlyEmployee(const std::string &name,
            const SSN &ssn, const int &rate, const int &hours)
:  Employee(name, ssn), m_rate(rate), m_hours(hours)
{//preconditions
    BOOST_ASSERT_MSG(rate>=0, "rate");
    BOOST_ASSERT_MSG(hours>=0, "hours");
    //postconditions
    BOOST_ASSERT_MSG(m_rate ==rate, "rate stored");
    BOOST_ASSERT_MSG(m_hours ==hours, "hours stored");}
    int HourlyEmployee::pay() const
{return m_rate*m_hours;}





