
// // SPDX-License-Identifier: GPL-3.0-or-later

#include <vector>
#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <assertexcept.h>
#include <employee.h>
#include <salariedemployee.h>
#include <hourlyemployee.h>
int main() {
try {
    std::vector<Employee*> payroll;
    Employee* employee = new SalariedEmployee("Nadine Wright\t", SSN("886-03-0004"), 4477);
    payroll.push_back(employee);
    employee = new SalariedEmployee("Joanna Wright\t", SSN("428-62-6367"), 8973);
    payroll.push_back(employee);
    employee = new HourlyEmployee("Richard Yearsley\t", SSN("520-73-4170"), 15, 26);
    payroll.push_back(employee);
    employee = new HourlyEmployee("James Wilkins\t", SSN("437-97-3377"),  17, 147);
    payroll.push_back(employee);
    for(size_t i=0; i<payroll.size(); ++i) {
        std::cout << "Pay to the order of " << payroll[i]->name()<< '$' << payroll[i]->pay() << '\n';}
    for(size_t i=0; i<payroll.size(); ++i)
        delete payroll[i];
    return 0;}// build and process payroll
catch (AssertException& e) {std::cerr << "Assertion Exception caught: " << e.what() << '\n';
    return EXIT_FAILURE;}
catch (std::runtime_error& e) {
    std::cerr << "Standard exception caught: " << e.what() << '\n';
    return EXIT_FAILURE;}
catch (...) {std::cerr << "Unknown exception caught\n";
    return EXIT_FAILURE;}
return EXIT_SUCCESS;}