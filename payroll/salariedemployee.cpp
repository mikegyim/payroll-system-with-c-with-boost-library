
// SPDX-License-Identifier: GPL-3.0-or-later
#include <salariedemployee.h>
#include <assertexcept.h>
#include "../ssn/ssn.h"
bool SalariedEmployee::invariant() const {
    BOOST_ASSERT(Employee::invariant());
    BOOST_ASSERT_MSG(m_salary >= 0, "Salary");
    return true;}
SalariedEmployee::SalariedEmployee( const std:: string  &name,
        const SSN &ssn,  const int& salary): Employee(name, ssn ), m_salary(salary)
{//preconditions
    BOOST_ASSERT_MSG(salary >=0, "salary" );
    //postconditions
    BOOST_ASSERT_MSG(m_salary == salary, "salary stored" );}
    int SalariedEmployee::pay() const
{return m_salary;}




